#include "form_item_data.h"
#include "ui_form_item_data.h"

Form_Item_Data::Form_Item_Data(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form_Item_Data)
{
    ui->setupUi(this);
}

Form_Item_Data::~Form_Item_Data()
{
    delete ui;
}

void Form_Item_Data::set_title(QString title)
{
    ui->groupBox->setTitle(title);
    if(_title.isEmpty()) _title=title;
}

void Form_Item_Data::update(const interface_data *_data)
{
    ui->label_accx->setText("X:"+qs_float(_data->_accel_x) + " m/s^2");
    ui->label_accy->setText("Y:"+qs_float(_data->_accel_y) + " m/s^2");
    ui->label_accz->setText("Z:"+qs_float(_data->_accel_z) + " m/s^2");
    ui->label_gyrox->setText("X:"+qs_float(_data->_gyro_x) + " °/s");
    ui->label_gyroy->setText("Y:"+qs_float(_data->_gyro_y) + " °/s");
    ui->label_gyroz->setText("Z:"+qs_float(_data->_gyro_z) + " °/s");
    ui->label_mbar->setText(qs_float(_data->pressure) + " mbar");
    ui->label_Tbar->setText(qs_float(_data->temperature) + " ℃");
    ui->label_Tacc->setText(qs_float(_data->_last_temperature) + " ℃");

    ui->groupBox->setTitle(_title+"_"+qs_hex(_data->acc_whoami));
}

QString Form_Item_Data::qs_float(const float value)
{
    char buffer[32];
    memset(buffer, 0, sizeof(buffer));
    sprintf(buffer, "%2.3f", value);
    return QString::fromLocal8Bit(buffer);
}

QString Form_Item_Data::qs_hex(const uint8_t addr)
{
    char buffer[32];
    memset(buffer, 0, sizeof(buffer));
    sprintf(buffer, "0x%02X", addr);
    return QString::fromLocal8Bit(buffer);
}
