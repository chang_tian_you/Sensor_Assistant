/******************** (C) COPYRIGHT 2018 merafour ********************
* Author             : 冷月追风@merafour.blog.163.com
* Version            : V1.0.0
* Date               : 8/1/2019
* Description        : 窗口管理与调度.
********************************************************************************
* merafour.blog.163.com
* merafour@163.com
* github.com/Merafour
*******************************************************************************/
#ifndef SCHEDULING_H
#define SCHEDULING_H

#include <QObject>
#include <QProgressBar>
#include <QGroupBox>

#include "ipmc.h"
#include "QiSerial/scan_device.h"
#include "form_item_data.h"
#include "handle/thread_handle.h"
#include "form_parameter.h"

class MainWindow;
class scheduling : public QObject
{
    Q_OBJECT
public:
    explicit scheduling(QProgressBar *_progress, QGroupBox *_groupbox, QObject *parent = nullptr);

    //void init(QProgressBar *_progress);
    int add_form(Form_Item_Data* _form, const int _index)
    {
        int _max = sizeof (item_form)/sizeof (item_form[0]);
        if(_index>form_max) return  -1;
        item_form[_index] = _form;
        form_max++;
        if(form_max>=_max) form_max = _max-1;
        return  0;
    }
    void set_form_param(Form_Parameter *_param)
    {
        _Parameter = _param;
    }
    void write_param(struct check_rang_arg * _arg);
    void set_titel(void)
    {
        for(int i=0; i<form_max; i++)
        {
            item_form[i]->set_title(tr("Data") + QString::number(i+1));
        }
    }
    void Disconnect(const uint8_t flag)
    {
        //Scanning::scanning(flag);
        _scanning->scanning(flag);
        if(0==flag)
        {
#if 0
            uint8_t i=0;
            for(i=0; i<form_max; i++)
            {
                item_form[i]->stop();
            }
#else
            _thread_handle->stop();
#endif
        }
        _is_work = flag;
    }
    void AutoConnect(const uint8_t flag)
    {
        _AutoConnect = flag;
        Disconnect(flag);
    }
    void search_device(void)
    {
#if 0
        uint8_t i=0;
        for(i=0; i<form_max; i++)
        {
            item_form[i]->search_device();
        }
#endif
    }
    void Language(QTranslator &_t);
    int idle(QString COM);
    uint8_t busy(void)
    {
        return _busy;
    }

signals:

public slots:

private slots:
    // 设备操作
    //void slots_progress_update(const QString &titel, int value);
    void slots_scanning_update(const QString &titel, int value);
    void slots_progress_update(const QString &titel, int value);

private:
    void init(QProgressBar *_progress);

    Form_Item_Data*  item_form[20];
    int form_max;
    uint8_t _AutoConnect;
    uint8_t _is_work;
    uint8_t _busy;
    Form_Parameter *_Parameter;

    QProgressBar *progress;
    QGroupBox *groupbox;
    QString qss_default;
    ScanningDevice *_scanning;
    thread_handle_controller *_thread_handle;
    struct interface_data *_interface_data;
    qint64 timestamp;
};

#endif // SCHEDULING_H
