#ifndef INTERFACE_SENSOR_H
#define INTERFACE_SENSOR_H

#include <QObject>
#include <stdio.h>
#include <string.h>
#include <QDebug>

#include "QiSerial/qimsgsync.h"

struct interface_data
{
    float _accel_x;
    float _accel_y;
    float _accel_z;
    float _gyro_x;
    float _gyro_y;
    float _gyro_z;
    float _last_temperature;
    float pressure;
    float temperature;
    uint8_t acc_whoami;
};
struct check_rang_arg
{
    uint32_t mbar_min;
    uint32_t mbar_max;
    uint32_t accel_min;
    uint32_t accel_max;
    uint32_t temp_min;
    uint32_t temp_max;
    uint32_t temp_diff;
};

class interface_sensor : public QiMSGSync
{
    Q_OBJECT
public:
    //explicit interface_sensor(QObject *parent = nullptr);
    explicit interface_sensor(QiMSGPort &_port) : QiMSGSync(nullptr, _port)
    {
        ;
    }
    enum IPMC
    {
        // response codes
        NONE    = 0x70,    // NONE CMD
        SERACH  = 0x71,    // serach device
        MSG2DEV = 0x72,    // msg send to device
        MSG2PC  = 0x73,    // msg send to pc
    };
    void float_to_buf(uint8_t* buf, const float value)
    {
        const uint8_t* _value = (const uint8_t*)(&value);
        memcpy(buf, _value, 4);
    }
    void float_from_buf(const uint8_t* buf, float* value)
    {
        uint8_t* _value = (uint8_t*)(value);
        memcpy(_value, buf, 4);
    }
    int msg_from_pc(const uint8_t slave_addr, const uint8_t _msg, const uint8_t _eoc, const uint8_t buf[], const uint8_t lenght);
    int msg_send2_pc(const uint8_t _msg, const uint8_t _ok, const uint8_t _eoc, uint8_t name[16], uint8_t buf[], const uint8_t lenght);

signals:

public slots:
};

#endif // INTERFACE_SENSOR_H
