/******************** (C) COPYRIGHT 2018 merafour ********************
* Author             : 冷月追风@merafour.blog.163.com
* Version            : V1.0.0
* Date               : 8/1/2019
* Description        : 设备通讯接口.
********************************************************************************
* merafour.blog.163.com
* merafour@163.com
* github.com/Merafour
*******************************************************************************/
#ifndef INTERFACE_IPMC_H
#define INTERFACE_IPMC_H

#include <QObject>
#include <stdio.h>
#include <string.h>
#include <QDebug>

#include "QiSerial/qimsgsync.h"
class interface_ipmc : public QiMSGSync
{
    Q_OBJECT
public:
    //explicit interface_ipmc(QObject *parent = nullptr);
    explicit interface_ipmc(QiMSGPort &_port) : QiMSGSync(nullptr, _port)
    {
        ;
    }
//    enum Code
//    {
//        // response codes
//        SE    = 0x0180,
//        CRC   = 0x0181,
//        SCAN  = 0x0182,
//        TIME  = 0x0183,
//        ERR   = 0x0184,
//        PULL  = 0x0185,
//        MSG   = 0x0186,
//    };
    enum IPMC
    {
        // response codes
        NONE    = 0x70,    // NONE CMD
        SERACH  = 0x71,    // serach device
        MSG2DEV = 0x72,    // msg send to device
        MSG2PC  = 0x73,    // msg send to pc
    };
    int msg_from_pc(const uint8_t slave_addr, const uint8_t _msg, const uint8_t _eoc, const uint8_t buf[], const uint8_t lenght);
    int msg_send2_pc(const uint8_t _msg, const uint8_t _ok, const uint8_t _eoc, uint8_t name[16], uint8_t buf[], const uint8_t lenght);

};

#endif // INTERFACE_IPMC_H
