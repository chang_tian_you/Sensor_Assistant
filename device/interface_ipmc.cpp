/******************** (C) COPYRIGHT 2018 merafour ********************
* Author             : 冷月追风@merafour.blog.163.com
* Version            : V1.0.0
* Date               : 8/1/2019
* Description        : 设备通讯接口.
********************************************************************************
* merafour.blog.163.com
* merafour@163.com
* github.com/Merafour
*******************************************************************************/
#include "interface_ipmc.h"

//interface_ipmc::interface_ipmc(QObject *parent) : interface_general (parent)
//{

//}
#if 0
int interface_ipmc::msg_from_pc(const uint8_t slave_addr, const uint8_t _msg, const uint8_t _eoc, const uint8_t buf[], const uint8_t lenght)
{
    /**
    format: |sync     |len|data     |sum   |EOC   |
    byte  : | 0  | 1  |2  | n+2     | n+2+l| n+3+l|
    stream: |MSG |EOC |len|data .l. | sum  |EOC   |
    */
    static uint8_t pack[256];
    uint8_t _lenght=0;
    uint8_t index=0;
    uint8_t sum=0;
    index=0;
    pack[index++] = _msg;
    pack[index++] = _eoc;
    pack[index++] = lenght+1;
    pack[index++] = slave_addr;
    for(_lenght=0; _lenght<lenght; _lenght++)
    {
        pack[index++] = buf[_lenght];
    }
    // check sum
    sum=0;
    for(_lenght=0; _lenght<index; _lenght++)
    {
        sum += pack[_lenght];
    }
    pack[index++] = sum;
    pack[index++] = _eoc;
    //__sync();
    // send
    this->clear();
    QThread::msleep(1);
    __send(pack, index);
    //QThread::msleep(1);
    have_data(100);
    //have_data(1);
    return __getSync();
}

int interface_ipmc::msg_send2_pc(const uint8_t _msg, const uint8_t _ok, const uint8_t _eoc, uint8_t name[], uint8_t buf[], const uint8_t lenght)
{
    /**
    format: |sync     |len|name     |data     |sum   |EOC   |
    byte  : | 0  | 1  |2  | 3       | n+3     | n+3+l| n+4+l|
    stream: |MSG | OK |len|name .n. |data .l. | sum  |EOC   |
    */
    static uint8_t pack[256];
    uint8_t _lenght=0;
    uint8_t index=0;
    uint8_t sum=0x11;
    uint8_t sum2=0x22;
    uint8_t len;
    index=0;
    have_data(100);
    pack[index++] = __recv();
    pack[index++] = __recv();
    //qDebug("_msg:0x%02X [0x%02X] _eoc:0x%02X [0x%02X] ", pack[0], _msg, pack[1], _ok);
    if((_msg!=pack[0]) || (_ok!=pack[1])) goto bad;
    len = __recv();
    //qDebug("_msg:0x%02X _eoc:0x%02X len:%d [%d]", pack[0], pack[1], len, lenght);
    if(len>lenght) goto bad;
    pack[index++] = len;
    for(_lenght=0; _lenght<len; _lenght++)
    {
        pack[index++] = __recv();
    }
    sum2 = __recv();
    //qDebug("sum2:0x%02X ", sum2);
    if(_eoc != __recv()) goto bad;
    // check sum
    sum=0;
    for(_lenght=0; _lenght<index; _lenght++)
    {
        sum += pack[_lenght];
    }
    if(sum2!=sum) goto  bad;
    // size_t __cdecl strlen(const char *_Str);
    //len = strlen(static_cast<const char*>(&pack[3]));
    //len = strlen(&pack[3]);
    len = static_cast<uint8_t>( strlen(reinterpret_cast<const char *>(&pack[3])) );
    memcpy(name, &pack[3], len);
    memcpy(buf, &pack[3+len+1], index-3-len); // 1byte '\0'
    //qDebug("msg recv ok! name:%s", name);
    //return static_cast<int>(index-3-len);
    return  (index-3-len-1); // 1byte '\0'
bad:
    return -1;
}
#else
int interface_ipmc::msg_from_pc(const uint8_t slave_addr, const uint8_t _msg, const uint8_t _eoc, const uint8_t buf[], const uint8_t lenght)
{
    /**
    format: |sync     |len|data     |sum   |EOC   |
    byte  : | 0  | 1  |2  | n+2     | n+2+l| n+3+l|
    stream: |MSG |EOC |len|data .l. | sum  |EOC   |
    */
    static uint8_t pack[256];
    uint8_t _lenght=0;
    uint8_t index=0;
    uint8_t sum=0;
    index=0;
    pack[index++] = _msg;
    pack[index++] = _eoc;
    pack[index++] = lenght+1;
    pack[index++] = slave_addr;
    for(_lenght=0; _lenght<lenght; _lenght++)
    {
        pack[index++] = buf[_lenght];
    }
    // check sum
    sum=0;
    for(_lenght=0; _lenght<index; _lenght++)
    {
        sum += pack[_lenght];
    }
    pack[index++] = sum;
    pack[index++] = _eoc;
    //qDebug("addr:0x%02X", index);
    //qDebug("pack: %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X", pack[0], pack[1], pack[2], pack[3], pack[4]
      //      , pack[5], pack[6], pack[7], pack[8], pack[9], pack[10], pack[11], pack[12]);
    //__sync();
    // send
    _Port.clear();
    QThread::msleep(1);
    __send(pack, index);
    //QThread::msleep(1);
    _Port.watting_data(100);
    //have_data(1);
    return __getSync();
}

int interface_ipmc::msg_send2_pc(const uint8_t _msg, const uint8_t _ok, const uint8_t _eoc, uint8_t name[], uint8_t buf[], const uint8_t lenght)
{
    /**
    format: |sync     |len|name     |data     |sum   |EOC   |
    byte  : | 0  | 1  |2  | 3       | n+3     | n+3+l| n+4+l|
    stream: |MSG | OK |len|name .n. |data .l. | sum  |EOC   |
    */
    static uint8_t pack[256];
    uint8_t _lenght=0;
    uint8_t index=0;
    uint8_t sum=0x11;
    uint8_t sum2=0x22;
    uint8_t len;
    index=0;
    _Port.watting_data(100);
    pack[index++] = __recv();
    pack[index++] = __recv();
    //qDebug("_msg:0x%02X [0x%02X] _eoc:0x%02X [0x%02X] ", pack[0], _msg, pack[1], _ok);
    if((_msg!=pack[0]) || (_ok!=pack[1])) goto bad;
    len = __recv();
    //qDebug("_msg:0x%02X _eoc:0x%02X len:%d [%d]", pack[0], pack[1], len, lenght);
    if(len>lenght) goto bad;
    pack[index++] = len;
    for(_lenght=0; _lenght<len; _lenght++)
    {
        pack[index++] = __recv();
    }
    sum2 = __recv();
    //qDebug("sum2:0x%02X ", sum2);
    if(_eoc != __recv()) goto bad;
    // check sum
    sum=0;
    for(_lenght=0; _lenght<index; _lenght++)
    {
        sum += pack[_lenght];
    }
    if(sum2!=sum) goto  bad;
    // size_t __cdecl strlen(const char *_Str);
    //len = strlen(static_cast<const char*>(&pack[3]));
    //len = strlen(&pack[3]);
    len = static_cast<uint8_t>( strlen(reinterpret_cast<const char *>(&pack[3])) );
    memcpy(name, &pack[3], len);
    memcpy(buf, &pack[3+len+1], index-3-len); // 1byte '\0'
    //qDebug("msg recv ok! name:%s", name);
    //return static_cast<int>(index-3-len);
    return  (index-3-len-1); // 1byte '\0'
bad:
    return -1;
}
#endif
