#-------------------------------------------------
#
# Project created by QtCreator 2019-01-13T13:40:41
#
#-------------------------------------------------

QT       += core gui xml serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = Sensor_Assistant
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    QiSerial/qimsgport.cpp \
    QiSerial/qimsgsync.cpp \
    QiSerial/qiqueue.cpp \
    QiSerial/qiserialport.cpp \
    QiSerial/qiserialportbuffer.cpp \
    QiSerial/qiserialsync.cpp \
    QiSerial/scan_device.cpp \
    oscillogram.cpp \
    qcustomplot.cpp \
    scheduling.cpp \
    menu/about.cpp \
    menu/qitranslator.cpp \
    menu/version.c \
    cJSON/cjsonformat.cpp \
    cJSON/cjsonstorage.cpp \
    cJSON/storage.cpp \
    cJSON/cJSON.c \
    ipmc.cpp \
    thread_ipmb.cpp \
    device/interface_ipmc.cpp \
    device/ipmc_protocol.cpp \
    form_thread.cpp \
    form_item_data.cpp \
    handle/thread_handle.cpp \
    device/interface_sensor.cpp \
    form_parameter.cpp

HEADERS += \
        mainwindow.h \
    QiSerial/qimsgport.h \
    QiSerial/qimsgsync.h \
    QiSerial/qiqueue.h \
    QiSerial/qiserialport.h \
    QiSerial/qiserialportbuffer.h \
    QiSerial/qiserialsync.h \
    QiSerial/scan_device.h \
    oscillogram.h \
    qcustomplot.h \
    scheduling.h \
    menu/about.h \
    menu/qitranslator.h \
    cJSON/cJSON.h \
    cJSON/cjsonformat.h \
    cJSON/cjsonstorage.h \
    cJSON/storage.h \
    ipmc.h \
    thread_ipmb.h \
    device/interface_ipmc.h \
    device/ipmc_protocol.h \
    form_thread.h \
    form_item_data.h \
    handle/thread_handle.h \
    device/interface_sensor.h \
    form_parameter.h

FORMS += \
        mainwindow.ui \
    oscillogram.ui \
    menu/about.ui \
    ipmc.ui \
    form_thread.ui \
    form_item_data.ui \
    form_parameter.ui
