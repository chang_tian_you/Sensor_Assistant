#include "mainwindow.h"
#include <QApplication>
#include <QProcess>

#if 0
#include <QFile>
#define DEBUG_INFO_FILE  1
#else
#define DEBUG_INFO_FILE  0
#endif
#if DEBUG_INFO_FILE
#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
void customMessageHandler(QtMsgType type, const QMessageLogContext &, const QString & str)
{
    QString txt=str;
    (void)type;
#else
void customMessageHandler(QtMsgType type, const char *msg)
{
    QString txt(msg);
#endif
    QFile outFile("debug.log");
    outFile.open(QIODevice::WriteOnly | QIODevice::Append);
    QTextStream ts(&outFile);
    ts << txt << endl;
}
#endif

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
#if DEBUG_INFO_FILE
#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
    qInstallMessageHandler(customMessageHandler);
#else
    qInstallMsgHandler(customMessageHandler);
#endif
#endif

    MainWindow w;
    w.show();

#if 0
    return a.exec();
#else
    int ret = a.exec();
    if(7714==ret)
    {
        QProcess::startDetached(qApp->applicationFilePath(), QStringList());
        return 0;
    }
    return ret;
#endif
}
