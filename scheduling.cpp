/******************** (C) COPYRIGHT 2018 merafour ********************
* Author             : 冷月追风@merafour.blog.163.com
* Version            : V1.0.0
* Date               : 8/1/2019
* Description        : 窗口管理与调度.
********************************************************************************
* merafour.blog.163.com
* merafour@163.com
* github.com/Merafour
*******************************************************************************/
#include "scheduling.h"
#include <QDebug>

scheduling::scheduling(QProgressBar *_progress, QGroupBox *_groupbox, QObject *parent) : QObject(parent)
{
    form_max = 0;
    _is_work = 0;
    groupbox = _groupbox;
    qss_default = groupbox->styleSheet();
    _busy = 0;
    _Parameter = nullptr;
    init(_progress);
}

void scheduling::write_param(struct check_rang_arg *_arg)
{
    memcpy(thread_handle::param(), _arg, sizeof(struct check_rang_arg));
    _thread_handle->write_param();
}
// 设备扫描子线程消息处理
void scheduling::init(QProgressBar *_progress)
{
    progress = _progress;
#if 0
    _scanning = new ControllerScanning();
    //connect(_scanning, SIGNAL(sig_Serial_Idle(const QString)), this, SLOT(slots_Serial_Idle(const QString)));
    //connect(_scanning, SIGNAL(sig_progress_update(const QString &, int)), this, SLOT(slots_progress_update(const QString &, int)));
    //connect(_scanning, SIGNAL(sig_progress_update(const QString &, int)), this, SLOT(slots_scanning_update(const QString &, int)));
    connect(_scanning, SIGNAL(sig_scanning_update(const QString &, int)), this, SLOT(slots_scanning_update(const QString &, int)));
    _scanning->Serial_Free("COM6");
    //_scanning->start();
    //Scanning::scanning(1);
#else
    _scanning = new ScanningDevice();
    connect(_scanning, SIGNAL(sig_scanning_update(const QString &, int)), this, SLOT(slots_scanning_update(const QString &, int)));
    _scanning->Serial_Free("COM6");
    _scanning->start();
    //_scanning->scanning(1);
#endif
    _interface_data = new interface_data[32];
    _thread_handle = new thread_handle_controller(_interface_data);
    connect(_thread_handle, SIGNAL(sig_progress_update(const QString &, int)), this, SLOT(slots_progress_update(const QString &, int)));
}

void scheduling::Language(QTranslator &_t)
{
    uint8_t i=0;
    qApp->installTranslator(&_t);
    for(i=0; i<form_max; i++)
    {
        item_form[i]->Language(_t);
    }
}

int scheduling::idle(QString COM)
{
    QString _t;
    _t = groupbox->title();
    if(0==_t.compare(COM))
    {
        groupbox->setEnabled(false);
        //ui->m_progressBar->setStyleSheet(qss_default);
        groupbox->setStyleSheet(QLatin1String("background-color: rgb(255, 255, 0);\n" "border-color: rgb(255, 255, 255);"));
        groupbox->setTitle("COM");
        progress->setValue(0);
        return 0;
    }
    return -1;
}

void scheduling::slots_scanning_update(const QString &titel, int value)
{
    switch (value) {
    case scan_device::Code::PULL:
        qDebug()<< "pull out:" << titel;
//        ui->m_progressBar->setStyleSheet(qss_default);
//        ui->m_progressBar->setFormat(titel);
#if 0
        for(int i=0; i<form_max; i++)
        {
            ipmc_form[i]->idle(titel);
        }
#else
        idle(titel);
#endif
        break;
    case scan_device::Code::SCAN:
        progress->setFormat(titel);
        break;
    case scan_device::Code::IDEL:
        {
            const QString COM = titel;
            qDebug() << "Serial Idle: " << COM;
            if(_is_work)
            {
#if 0
                uint8_t i=0;
                for(i=0; i<form_max; i++)
                {
                    if(0==ipmc_form[i]->busy())
                    {
                        ipmc_form[i]->handle(COM, _scanning);
                        return;
                    }
                }
#else
                if(0==busy())
                {
                    timestamp = QDateTime::currentDateTime().toMSecsSinceEpoch();
                    groupbox->setEnabled(true);
                    qDebug()<<QThread::currentThreadId();
                    groupbox->setTitle(COM);
                    progress->setStyleSheet(qss_default);
                    _busy = 1;
                    _thread_handle->start(COM);
                }
#endif
            }
            else
            {
                _scanning->Serial_Free(COM);
            }
        }
        break;
    default:
        break;
    }
}

void scheduling::slots_progress_update(const QString &titel, int value)
{
    switch (value) {
    case QiMSGSync::SingalCode::PULL:
        progress->setStyleSheet(qss_default);
        progress->setFormat(titel);
        break;
    case QiMSGSync::SingalCode::ERR:
        progress->setValue(0);
        progress->setStyleSheet(QLatin1String("background-color: rgb(255, 0, 0);\n" "border-color: rgb(255, 255, 255);"));
        progress->setFormat(titel);
        break;
    case QiMSGSync::SingalCode::DOWN:
        //Scanning::Serial_Free(titel); // 释放串口资源
        //if(nullptr!=_scanning) this->_scanning->Serial_Free(titel);
        if(nullptr!=_scanning) this->_scanning->Serial_Free(titel);
//        _ipmc_protocol.list_clear_addr();
//        ui->groupBox_IPMC->setEnabled(false);
        _busy = 0;
        break;
    case QiMSGSync::SingalCode::PARAM:
        if(nullptr!=_Parameter)
        {
            _Parameter->update_parameter(thread_handle::param());
        }
        break;
    case QiMSGSync::SingalCode::TIME:
        progress->setFormat(progress->text()+titel);
        break;
    case QiMSGSync::SingalCode::REPO:  // Reported
        //qDebug("decode_send_cpu: %s", titel);
        //progress->setFormat(titel);
        //qDebug() << "slave_addr: " << titel;
        //ipmc_data_handle(titel);
        {
            uint8_t i=0;
            qint64 _timestamp;
            _timestamp = QDateTime::currentDateTime().toMSecsSinceEpoch();
            if(_timestamp<(timestamp+300)) break;
            timestamp = _timestamp;
            for(i=0; i<form_max; i++)
            {
                item_form[i]->update(&_interface_data[i]);
            }
        }
        break;
    case QiMSGSync::SingalCode::MSG:
    default:
        progress->setValue(value);
        //QString str = titel+QString::number(value)+"%";
        //progress->setFormat(str);
        progress->setFormat(titel);
        break;
    }
}

