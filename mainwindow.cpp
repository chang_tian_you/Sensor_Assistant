#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QSettings>
#include <QDebug>
#include <QTranslator>
#include <QFileDialog>
#include <QDesktopWidget>
#include "menu/about.h"
#include <QFileDialog>
#include "qcustomplot.h"
const char* MainWindow::translator_table[] =
{
    // title
    "Sensor Assistant",
    // Language
    "Language", "English", "Chinese", "Switch to English", "Switch to Simplified Chinese",
    "disconnect of device", "Language.txt file switch Language",
    // menu, File
    "File", "Cutover", "Cutover data file format", "Oscillogram", "Oscillogram show data",
    // menu, Operate
    "Operate", "AutoConnect", "Connect", "Disconnect", "Search", "Search IPMC Device", "Auto Connect Device",
    "Write Param", "Write Param to device",
    // menu, About
    "About", "About IPMB Assistant",
    "Help"
};

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    QSettings setting("./Setting.ini", QSettings::IniFormat);
    int count;
    bool ok;
    count = setting.value("count").toInt(&ok);
    qDebug() << "count:" << count;
    setting.setValue("count", QString::number(++count));
    setWindowFlags(Qt::WindowCloseButtonHint);
    ui->setupUi(this);
    //ui->widget_IPMC2->setVisible(false);
//    ui->widget_IPMC3->setVisible(false);
//    ui->widget_IPMC4->setVisible(false);
    ui->label->setVisible(false);

    _cutover = new QiTranslator("MainWindow", translator_table, sizeof (translator_table)/sizeof (translator_table[0]));
    _cutover->load();

    _oscillogram = new Oscillogram();
    progress = new QProgressBar(this);
    _scheduling = new scheduling(progress, ui->groupBox, this);
    _scheduling->set_form_param(ui->widget_Param);
    //_scheduling->init((progress));
    _scheduling->add_form(ui->widget_form1, 0);
    _scheduling->add_form(ui->widget_form2, 1);
    _scheduling->add_form(ui->widget_form3, 2);
    _scheduling->add_form(ui->widget_form4, 3);
    _scheduling->add_form(ui->widget_form5, 4);
    _scheduling->add_form(ui->widget_form6, 5);
    _scheduling->add_form(ui->widget_form7, 6);
    _scheduling->add_form(ui->widget_form8, 7);
    _scheduling->set_titel();
    // 语言转换与菜单
    Language_cutover();
    //setWindowTitle("IPMB Assistant");
    setWindowTitle(_cutover->cutover("Sensor Assistant"));
#if 0
    create_menu();
#else // 使用 JSON 文件转换语言
    create_menu_json();
#endif
    ui->menuLanguage->setEnabled(false);
    // 设置窗体大小
    this->adjustSize();
    _width = this->width();
    _height = this->height();
    setFixedSize(this->width(), this->height());
    //ui->groupBox_download->setFixedSize(ui->groupBox_download->width(), ui->groupBox_download->height());
    //setFixedSize(this->width(), this->height());
    setWindowFlags(windowFlags() &~ Qt::WindowMaximizeButtonHint);
#if 1
    // 设置状态栏
    permanent = new QLabel(this);
    //permanent->setFrameShape(QFrame::WinPanel);
    //permanent->setFrameShadow(QFrame::Sunken);
    //permanent->setFrameStyle(QFrame::Box | QFrame::Sunken);
    permanent->setText(tr("<a href=\"http://github.com/Merafour\">github.com/Merafour</a>"));
    permanent->setTextFormat(Qt::RichText);
    permanent->setOpenExternalLinks(true);
    ui->statusBar->addPermanentWidget(permanent);
#if 0
    msgLabel = new QLabel(this);
    ui->statusBar->addWidget(msgLabel);
    msgLabel->setMinimumWidth(150);
#endif
    //progress = new QProgressBar(this);
    progress->setAlignment(Qt::AlignRight | Qt::AlignVCenter);  // 对齐方式
    qss_default = progress->styleSheet(); // 保存样式
    //progress->setMinimumWidth(_width-permanent->width()-30-msgLabel->width());
    progress->setMinimumWidth(_width-permanent->width()-30);
    progress->setRange(0, 100);
    progress->setValue(0);
    ui->statusBar->addWidget(progress);
    statusBar()->setStyleSheet(QString("QStatusBar::item{border:0px}"));
    //statusBar()->setSizeGripEnabled(false);
#endif
    // 移动窗口位置
    //const QRect deskRect = QApplication::desktop()->availableGeometry();
    const QRect deskRect = QApplication::desktop()->availableGeometry(this);
    //this->move(deskRect.left(), deskRect.top());
    this->move((deskRect.width()-_width)/2, (deskRect.height()-_height)/2-(deskRect.height()-_height)/3);

    //ui->statusBar->show();
    ui->statusBar->showMessage("IPMB Assistant", 3000);
}

MainWindow::~MainWindow()
{
    //Scanning::exit_thread();
    QThread::msleep(100);
    delete _cutover;
    delete ui;
}

void MainWindow::actionLanguage_triggered()
{
    // 切换语言
    QSettings setting("./Setting.ini", QSettings::IniFormat);
    //QAction *action = (QAction*)sender();
    QAction *action = reinterpret_cast<QAction *>(sender());
    QString actionText = action->text();
    setting.setValue(Setting_Language, actionText);
    qApp->exit(7714); // main 函数通过 7714 判断程序需要重启
}
#include "cJSON/cjsonformat.h"
void MainWindow::actionOperate_triggered()
{
    QString objectName = sender()->objectName();
    //qDebug()<<"objectName:"<<objectName<<endl;
    //if(0==objectName.compare("actionAbout"))
    if(0==objectName.compare(actionDisconnect->objectName()))
    {
        //static uint8_t Connect_flag=1; // "Connect"
        //if(0==actionDisconnect->text().compare("Disconnect"))
        if(0==QString::compare(_cutover->cutover("Disconnect"), actionDisconnect->text()))
        //if(0==Connect_flag)
        {
            qDebug() << "Action Disconnect";
            actionDisconnect->setText(_cutover->cutover("Connect"));
            _scheduling->Disconnect(0);
            actionSearch->setEnabled(false);
            //Connect_flag = 1;
        }
        else
        {
            qDebug() << "Action Connect";
            actionDisconnect->setText(_cutover->cutover("Disconnect"));
            _scheduling->Disconnect(1);
            actionSearch->setEnabled(true);
            //Connect_flag = 0;
        }
    }
    if(0==objectName.compare(actionSearch->objectName()))
    {
        //qDebug() << "Action Search";
        _scheduling->search_device();
    }
    if(0==objectName.compare(actionAutoConnect->objectName()))
    {
        //qDebug() << "Action AutoConnect";
        _scheduling->AutoConnect(actionAutoConnect->isChecked());
        actionDisconnect->setEnabled(!actionAutoConnect->isChecked());
        actionSearch->setEnabled(actionAutoConnect->isChecked());
    }
    if(0==objectName.compare(actionCutover->objectName()))
    {
        //qDebug() << "Action Cutover";
        QString fileName = QFileDialog::getOpenFileName(this, _cutover->cutover("Open Data File"), "./", tr("Data Files (*.txt)"));
        //qDebug() << "fileName:" << fileName;
        if(fileName.isNull() || fileName.isEmpty()) return;
        QString fileSave = QFileDialog::getSaveFileName(this, _cutover->cutover("Save Data File"), "./", tr("Data Files (*.txt)"));
        qDebug() << "fileSave:" << fileSave;
        cJSONformat format;
        int ret=-1;
        ret = format.format_cutover(fileName.toLocal8Bit().data(), fileSave.toLocal8Bit().data());
        if(0==ret)
        {
            qDebug() << "save OK!";
        }
        else
        {
            qDebug() << "save fail!";
        }
    }
    if(0==objectName.compare(actionOscillogram->objectName()))
    {
        qDebug() << "Action Oscillogram";
        //Oscillogram *osc = new Oscillogram();
        _oscillogram->show();
    }
    if(0==objectName.compare(actionParam->objectName()))
    {
        qDebug() << "Action Param";
        struct check_rang_arg _arg;
        ui->widget_Param->parameter(&_arg);
        _scheduling->write_param(&_arg);
    }
}

void MainWindow::actionHelp_triggered()
{
#if 0
    QAction *action = (QAction*)sender();
    QString objectName = action->objectName();
#endif
    QString objectName = sender()->objectName();
    qDebug()<<"objectName:"<<objectName<<endl;
    //if(0==objectName.compare("actionAbout"))
    if(0==objectName.compare(actionAbout->objectName()))
    {
        About _about;
        _about.setWindowModality(Qt::ApplicationModal);
        _about.exec();
    }
}

// 创建菜单
//void MainWindow::create_menu()
//{
//}

void MainWindow::create_menu_json()
{
    ui->menuLanguage->setTitle(_cutover->cutover("Language"));
    ui->menuLanguage->setStatusTip(_cutover->cutover("Language.txt file switch Language"));
    ui->actionEnglish->setText(_cutover->cutover("English"));
    ui->actionChinese->setText(_cutover->cutover("Chinese"));
    ui->actionEnglish->setStatusTip(_cutover->cutover("Switch to English"));
    ui->actionChinese->setStatusTip(_cutover->cutover("Switch to Simplified Chinese"));
//    ui->actionEnglish->setShortcut(Qt::SHIFT | Qt::Key_E);
//    ui->actionSimplified_Chinese->setShortcut(Qt::SHIFT | Qt::Key_C);
    connect(ui->actionEnglish, SIGNAL(triggered()), this, SLOT(actionLanguage_triggered()));
    connect(ui->actionChinese, SIGNAL(triggered()), this, SLOT(actionLanguage_triggered()));
    // Menu_File
    actionCutover = new QAction(_cutover->cutover("Cutover"));
    actionCutover->setObjectName(QStringLiteral("actionCutover"));
    actionCutover->setCheckable(false);
    actionCutover->setStatusTip(_cutover->cutover("Cutover data file format"));
    actionOscillogram = new QAction(_cutover->cutover("Oscillogram"));
    actionOscillogram->setObjectName(QStringLiteral("actionOscillogram"));
    actionOscillogram->setCheckable(false);
    actionOscillogram->setStatusTip(_cutover->cutover("Oscillogram show data"));
    actionLoad = new QAction(tr("Load"));
    actionLoad->setObjectName(QStringLiteral("actionLoad"));
    actionLoad->setCheckable(false);
    actionLoad->setStatusTip(tr("Load music file"));
    Menu_File = new QMenu(_cutover->cutover("File"));
    Menu_File->setObjectName(QStringLiteral("actionFile"));
    actionCutover->setShortcut(Qt::ALT | Qt::Key_C);
    actionOscillogram->setShortcut(Qt::ALT | Qt::Key_S);
    actionLoad->setShortcuts(QKeySequence::Open);
    Menu_File->addAction(actionCutover);
    Menu_File->addAction(actionOscillogram);
    //Menu_File->addSeparator();
    //Menu_File->addAction(actionLoad);
    ui->menuBar->addAction(Menu_File->menuAction());
    connect(actionCutover, SIGNAL(triggered()), this, SLOT(actionOperate_triggered()));
    connect(actionOscillogram, SIGNAL(triggered()), this, SLOT(actionOperate_triggered()));
    connect(actionLoad, SIGNAL(triggered()), this, SLOT(actionOperate_triggered()));
    // Menu_Operate
    actionDisconnect = new QAction(_cutover->cutover("Connect"));
    actionDisconnect->setObjectName(QStringLiteral("actionDisconnect"));
    actionDisconnect->setCheckable(false);
    actionDisconnect->setStatusTip(_cutover->cutover("disconnect of device"));
    //actionSearch = new QAction(tr("Search"));
    actionSearch = new QAction(_cutover->cutover("Search"));
    actionSearch->setObjectName(QStringLiteral("actionSearch"));
    actionSearch->setCheckable(false);
    actionSearch->setStatusTip(_cutover->cutover("Search IPMC Device"));
    actionSearch->setEnabled(false);
    //actionAutoConnect = new QAction(tr("AutoConnect"));
    actionAutoConnect = new QAction(_cutover->cutover("AutoConnect"));
    actionAutoConnect->setObjectName(QStringLiteral("actionAutoConnect"));
    actionAutoConnect->setCheckable(true);
    actionAutoConnect->setStatusTip(_cutover->cutover("Auto Connect Device"));
    actionParam = new QAction(_cutover->cutover("Write Param"));
    actionParam->setObjectName(QStringLiteral("actionParam"));
    actionParam->setCheckable(false);
    actionParam->setStatusTip(_cutover->cutover("Write Param to device"));
    Menu_Operate = new QMenu(_cutover->cutover("Operate"));
    Menu_Operate->setObjectName(QStringLiteral("actionOperate"));
    actionDisconnect->setShortcut(Qt::CTRL | Qt::Key_D);
    //actionLoad->setShortcut(Qt::ALT | Qt::Key_L);
    actionSearch->setShortcut(Qt::CTRL | Qt::Key_S);
    actionAutoConnect->setShortcut(Qt::ALT | Qt::Key_A);
    actionParam->setShortcut(Qt::ALT | Qt::Key_P);
    Menu_Operate->addAction(actionDisconnect);
    Menu_Operate->addAction(actionSearch);
    Menu_Operate->addAction(actionAutoConnect);
    Menu_Operate->addAction(actionParam);
    ui->menuBar->addAction(Menu_Operate->menuAction());
    connect(actionDisconnect, SIGNAL(triggered()), this, SLOT(actionOperate_triggered()));
    connect(actionSearch, SIGNAL(triggered()), this, SLOT(actionOperate_triggered()));
    connect(actionAutoConnect, SIGNAL(triggered()), this, SLOT(actionOperate_triggered()));
    connect(actionParam, SIGNAL(triggered()), this, SLOT(actionOperate_triggered()));
    // Menu_Help
    actionAbout = new QAction(_cutover->cutover("About"));
    actionAbout->setObjectName(QStringLiteral("actionAbout"));
    actionAbout->setCheckable(false);
    //actionAbout->setShortcuts(QKeySequence::AddTab);
    actionAbout->setShortcut(Qt::ALT | Qt::Key_A);
    actionAbout->setStatusTip(_cutover->cutover("About IPMB Assistant"));
    Menu_Help = new QMenu(_cutover->cutover("Help"));
    Menu_Help->setObjectName(QStringLiteral("actionHelp"));
    Menu_Help->addAction(actionAbout);
    ui->menuBar->addAction(Menu_Help->menuAction());
    connect(actionAbout, SIGNAL(triggered()), this, SLOT(actionHelp_triggered()));
}
// 转换语言,需要生成语言转换文件 *.qm
void MainWindow::Language_cutover()
{
    QTranslator translator;
    bool load=false;
    (void)load;
    QString _language;
    QSettings setting("./Setting.ini", QSettings::IniFormat);
    _language = setting.value(Setting_Language).toString();
    qDebug() << "Language: " << _language;
    if(_language.isEmpty())
    {
        _language = ui->actionChinese->text();
        setting.setValue(Setting_Language, _language);
    }
    //if(0==_language.compare("Simplified Chinese"))
    if(0==_language.compare(ui->actionChinese->text()))
    {
        load=translator.load("cn.qm");
        ui->actionEnglish->setChecked(false);
        ui->actionChinese->setChecked(true);
    }
    //else if (0==_language.compare("English"))
    else if (0==_language.compare(ui->actionEnglish->text()))
    {
        load=translator.load("en.qm");
        ui->actionEnglish->setChecked(true);
        ui->actionChinese->setChecked(false);
    }
    else // English
    {
        load=translator.load("en.qm");
        ui->actionEnglish->setChecked(true);
        ui->actionChinese->setChecked(false);
        _language = ui->actionEnglish->text();
        setting.setValue(Setting_Language, _language);
    }
    ui->menuLanguage->setEnabled(true);
    ui->menuLanguage->setVisible(true);
    _scheduling->Language(translator);
}
