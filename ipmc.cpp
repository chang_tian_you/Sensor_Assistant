/******************** (C) COPYRIGHT 2018 merafour ********************
* Author             : 冷月追风@merafour.blog.163.com
* Version            : V1.0.0
* Date               : 8/1/2019
* Description        : 子窗口.
********************************************************************************
* merafour.blog.163.com
* merafour@163.com
* github.com/Merafour
*******************************************************************************/
#include "ipmc.h"
#include "ui_ipmc.h"
#include <QDebug>

const QString IPMC::label_item_data[20] = {
    /* 0 */   "板子类型:",
    /* 1 */   "板子温度:",
    /* 2 */   "28VDC:",
    /* 3 */   "12VDC_MAIN:",
    /* 4 */   "12VDC_AUX:",
    /* 5 */   "-12VDC_AUX:",
    /* 6 */   "5VDC_MAIN:",
    /* 7 */   "5VDC_AUX:",
    /* 8 */   "3V3VDC_MAIN:",
    /* 9 */   "3V3VDC_AUX:",
    /*10 */   "12V main 电流:",
    /*11 */   "12V aux 电流:",
    /*12 */   "5V main 电流:",
    /*13 */   "5V aux 电流:",
    /*14 */   "3V main 电流:",
    /*15 */   "3V aux 电流:",
    "NULL","NULL","NULL","NULL"
};

IPMC::IPMC(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::IPMC)
{
    uint8_t count=0;
    //_scanning = nullptr;
    _scanning_device = nullptr;
    ui->setupUi(this);
    //ui->listWidget->setViewMode(QListView::IconMode);   //设置显示模式为图标模式
    ui->listWidget_IPMC->setViewMode(QListView::ListMode);   //设置显示模式为列表模式
    ui->pushButton_1->setVisible(false);
    ui->label->setVisible(false);
    ui->label_2->setVisible(false);
    ui->listWidget_IPMC->clear();
    ui->groupBox_IPMC->setEnabled(false);
    for(count=0; count<35; count++)
    {
        new QListWidgetItem("IPMC"+QString::number(count+1), ui->listWidget_IPMC);
    }
    ui->listWidget_Data->clear();
    ui->listWidget_Data->setVisible(false);
    for(count=0; count<LABEL_ITEM_MAX; count++)
    {
        new QListWidgetItem(label_item_data[count], ui->listWidget_Data);
        lable_data[count] = new QLabel;
        lable_data[count]->setText(label_item_data[count]);
        lable_data[count]->setMinimumWidth(300);
        ui->verticalLayout_Data->addWidget(lable_data[count]);
    }

    ui->progressBar->setValue(0);
    qss_default = ui->groupBox_IPMC->styleSheet();
    ui->progressBar->setAlignment(Qt::AlignRight | Qt::AlignVCenter);  // 对齐方式
    ui->progressBar->setFormat(tr("Watting"));
    _busy = 0;
    ipmb_cpu = new thread_ipmb_controller(_ipmc_protocol);
    connect(ipmb_cpu, SIGNAL(sig_progress_update(const QString &, int)), this, SLOT(slots_progress_update(const QString &, int)));
}

IPMC::~IPMC()
{
    delete ui;
}

void IPMC::set_title(QString title)
{
    ui->groupBox_IPMC->setTitle(title);
    if(_title.isEmpty()) _title=title;
}

//void IPMC::handle(QString COM, ControllerScanning *_scan)
//{
//    _scanning = _scan;
//    _ipmc_protocol.list_clear_addr(); // 清空数据
//    timestamp = QDateTime::currentDateTime().toMSecsSinceEpoch();
//    ui->listWidget_IPMC->clear();
//    ui->groupBox_IPMC->setEnabled(true);
//    ipmb_cpu->start(COM);
//    qDebug()<<QThread::currentThreadId();
//    ui->groupBox_IPMC->setTitle(COM);
//    ui->progressBar->setStyleSheet(qss_default);
//    _busy = 1;
//}

void IPMC::handle(QString COM, ScanningDevice *_scan)
{
    _scanning_device = _scan;
    _ipmc_protocol.list_clear_addr();
    timestamp = QDateTime::currentDateTime().toMSecsSinceEpoch();
    ui->listWidget_IPMC->clear();
    ui->groupBox_IPMC->setEnabled(true);
    ipmb_cpu->start(COM);
    qDebug()<<QThread::currentThreadId();
    ui->groupBox_IPMC->setTitle(COM);
    ui->progressBar->setStyleSheet(qss_default);
    _busy = 1;
}

int IPMC::idle(QString COM)
{
    QString _t;
    _t = ui->groupBox_IPMC->title();
    if(0==_t.compare(COM))
    {
        ui->groupBox_IPMC->setEnabled(false);
        //ui->m_progressBar->setStyleSheet(qss_default);
        ui->progressBar->setStyleSheet(QLatin1String("background-color: rgb(255, 255, 0);\n" "border-color: rgb(255, 255, 255);"));
        ui->groupBox_IPMC->setTitle(_title);
        ui->progressBar->setValue(0);
        return 0;
    }
    return -1;
}

void IPMC::Language(QTranslator &_t)
{
    qApp->installTranslator(&_t);
    ui->retranslateUi(this);
}

void IPMC::on_listWidget_IPMC_clicked(const QModelIndex &index)
{
    (void)index;
    QListWidgetItem *item = ui->listWidget_IPMC->currentItem();
    qDebug() << "Index:" << ui->listWidget_IPMC->currentRow();
    qDebug() << "item:" << item->text();
    //qDebug("item addr: 0x%02X", list_ipmc_addr[ui->listWidget_IPMC->currentRow()]);
    //item->setText(item->text()+"a");
    //currentRow = static_cast<uint8_t>(ui->listWidget_IPMC->currentRow());
    _ipmc_protocol.list_set_row(static_cast<uint8_t>(ui->listWidget_IPMC->currentRow()));
    qDebug("item addr: 0x%02X", _ipmc_protocol.list_get_addr());
}

void IPMC::slots_progress_update(const QString &titel, int value)
{
    switch (value) {
    case QiMSGSync::SingalCode::PULL:
        ui->progressBar->setStyleSheet(qss_default);
        ui->progressBar->setFormat(titel);
        break;
    case QiMSGSync::SingalCode::ERR:
        ui->progressBar->setValue(0);
        ui->progressBar->setStyleSheet(QLatin1String("background-color: rgb(255, 0, 0);\n" "border-color: rgb(255, 255, 255);"));
        ui->progressBar->setFormat(titel);
        break;
    case QiMSGSync::SingalCode::DOWN:
        //Scanning::Serial_Free(titel); // 释放串口资源
        //if(nullptr!=_scanning) this->_scanning->Serial_Free(titel);
        if(nullptr!=_scanning_device) this->_scanning_device->Serial_Free(titel);
        _ipmc_protocol.list_clear_addr();
        ui->groupBox_IPMC->setEnabled(false);
        _busy = 0;
        break;
    case QiMSGSync::SingalCode::TIME:
        ui->progressBar->setFormat(this->ui->progressBar->text()+titel);
        break;
    case QiMSGSync::SingalCode::REPO:  // Reported
        //qDebug("decode_send_cpu: %s", titel);
        //ui->progressBar->setFormat(titel);
        //qDebug() << "slave_addr: " << titel;
        ipmc_data_handle(titel);
        break;
    case QiMSGSync::SingalCode::MSG:
    default:
        this->ui->progressBar->setValue(value);
        //QString str = titel+QString::number(value)+"%";
        //ui->progressBar->setFormat(str);
        ui->progressBar->setFormat(titel);
        break;
    }
}

void IPMC::ipmc_data_handle(QString slave)
{
    struct ipmc_data _data;
    uint8_t slave_addr = 0;
    //QLabel *label=nullptr;
    //uint8_t solt_id = 0;
    uint8_t index=0;
    uint8_t _item_id=0;
    qint64 _timestamp;
    slave_addr = static_cast<uint8_t>(slave.toInt());
    //qDebug("slave_addr: 0x%02X", slave_addr);
    insert_addr(slave_addr);
    _timestamp = QDateTime::currentDateTime().toMSecsSinceEpoch();
    //qDebug("_timestamp:%6d  [%d]", _timestamp, (_timestamp-timestamp));
    if(_timestamp<(timestamp+300)) return;
    timestamp = _timestamp;
    //qDebug("item[%d] addr: 0x%02X", ui->listWidget_IPMC->currentRow(), list_ipmc_addr[ui->listWidget_IPMC->currentRow()]);
    //solt_id = ipmc_protocolObj::get_solt_id(slave_addr);
    //slave_addr = list_ipmc_addr[currentRow];
    slave_addr = _ipmc_protocol.list_get_addr();
    ipmb_cpu->get_ipmc_data_faddr(_data, slave_addr);
    /**
const QString IPMC::label_item_data[20] = {
    "板子类型:", "板子温度:",
    "28VDC:",
    "12VDC_MAIN:","12VDC_AUX:","5VDC_MAIN:","5VDC_AUX:", "3V3VDC_MAIN:","3V3VDC_AUX:",
    "12V main 电流:","12V aux 电流:","5V main 电流:", "5V aux 电流:","3V main 电流:","3V aux 电流:",
    "NULL","NULL","NULL","NULL","NULL"
};
      */
    // "板子类型:"
    index=search_label_index("板子类型:");
    //label = search_label(label_item_data[index]);
    if(LABEL_ITEM_MAX>index)
    {
        const char* type = ipmc_protocolObj::get_device_type(_data.dev_type);
        QString _type(type);
        QString status;
        if(0x00 == _data.status) status = "开机";
        else if(0x00 == _data.status) status = "关机";
        else status = "未定义";
        //label->setText(label_item_data[0]+QString::number(_data.dev_type));
        //lable_data[index]->setText(label_item_data[index]+ "  " +_type);
        lable_data[index]->setText(_type);
        lable_data[index]->setText(lable_data[index]->text() + " 槽位：" + QString::number(_data.solt_id));
        lable_data[index]->setText(lable_data[index]->text() + " 状态：" + status);
    }
    // "板子温度:"
    index=search_label_index("板子温度:");
    if(LABEL_ITEM_MAX>index)
    {
        _item_id = ipmc_protocolObj::search_item_id("电源板上温度");
        lable_data[index]->setText(label_item_data[index]+ " " + QString("%1").arg(static_cast<double>(_data.item[_item_id].value))+"℃");
        _item_id = ipmc_protocolObj::search_item_id("MCU温度");
        lable_data[index]->setText(lable_data[index]->text() + " MCU温度: " + QString("%1").arg(static_cast<double>(_data.item[_item_id].value))+"℃");
    }
    // "28VDC:"
    index=search_label_index("28VDC:");
    if(LABEL_ITEM_MAX>index)
    {
        _item_id = ipmc_protocolObj::search_item_id("28VDC");
        lable_data[index]->setText(label_item_data[index]+ "  " +QString("%1").arg(static_cast<double>(_data.item[_item_id].value))+" V");
    }
    // "12VDC_MAIN:"
    index=search_label_index("12VDC_MAIN:");
    if(LABEL_ITEM_MAX>index)
    {
        _item_id = ipmc_protocolObj::search_item_id("12VDC_MAIN");
        lable_data[index]->setText(label_item_data[index]+ "  " +QString("%1").arg(static_cast<double>(_data.item[_item_id].value))+" V");
    }
    // "12VDC_AUX:"
    index=search_label_index("12VDC_AUX:");
    if(LABEL_ITEM_MAX>index)
    {
        _item_id = ipmc_protocolObj::search_item_id("12VDC_AUX");
        lable_data[index]->setText(label_item_data[index]+ "  " +QString("%1").arg(static_cast<double>(_data.item[_item_id].value))+" V");
    }
    // "-12VDC_AUX:"
    index=search_label_index("-12VDC_AUX:");
    if(LABEL_ITEM_MAX>index)
    {
        _item_id = ipmc_protocolObj::search_item_id("-12VDC_AUX");
        lable_data[index]->setText(label_item_data[index]+ "  " +QString("%1").arg(static_cast<double>(_data.item[_item_id].value))+" V");
    }
    // "5VDC_MAIN:"
    index=search_label_index("5VDC_MAIN:");
    if(LABEL_ITEM_MAX>index)
    {
        _item_id = ipmc_protocolObj::search_item_id("5VDC_MAIN");
        lable_data[index]->setText(label_item_data[index]+ "  " +QString("%1").arg(static_cast<double>(_data.item[_item_id].value))+" V");
    }
    // "5VDC_AUX:"
    index=search_label_index("5VDC_AUX:");
    if(LABEL_ITEM_MAX>index)
    {
        _item_id = ipmc_protocolObj::search_item_id("5VDC_AUX");
        lable_data[index]->setText(label_item_data[index]+ "  " +QString("%1").arg(static_cast<double>(_data.item[_item_id].value))+" V");
    }
    // "3V3VDC_MAIN:"
    index=search_label_index("3V3VDC_MAIN:");
    if(LABEL_ITEM_MAX>index)
    {
        _item_id = ipmc_protocolObj::search_item_id("3V3DC_MAIN");
        lable_data[index]->setText(label_item_data[index]+ "  " +QString("%1").arg(static_cast<double>(_data.item[_item_id].value))+" V");
    }
    // "3V3VDC_AUX:"
    index=search_label_index("3V3VDC_AUX:");
    if(LABEL_ITEM_MAX>index)
    {
        _item_id = ipmc_protocolObj::search_item_id("3V3DC_AUX");
        lable_data[index]->setText(label_item_data[index]+ "  " +QString("%1").arg(static_cast<double>(_data.item[_item_id].value))+" V");
    }
    // "12V main 电流:"
    index=search_label_index("12V main 电流:");
    if(LABEL_ITEM_MAX>index)
    {
        _item_id = ipmc_protocolObj::search_item_id("12V main 输出电流");
        lable_data[index]->setText(label_item_data[index]+ "  " +QString("%1").arg(static_cast<double>(_data.item[_item_id].value))+" A");
    }
    // "12V aux 电流:"
    index=search_label_index("12V aux 电流:");
    if(LABEL_ITEM_MAX>index)
    {
        _item_id = ipmc_protocolObj::search_item_id("12V aux 输出电流");
        lable_data[index]->setText(label_item_data[index]+ "  " +QString("%1").arg(static_cast<double>(_data.item[_item_id].value))+" A");
    }
    // "5V main 电流:"
    index=search_label_index("5V main 电流:");
    if(LABEL_ITEM_MAX>index)
    {
        _item_id = ipmc_protocolObj::search_item_id("5V main 输出电流");
        lable_data[index]->setText(label_item_data[index]+ "  " +QString("%1").arg(static_cast<double>(_data.item[_item_id].value))+" A");
    }
    // "5V aux 电流:"
    index=search_label_index("5V aux 电流:");
    if(LABEL_ITEM_MAX>index)
    {
        _item_id = ipmc_protocolObj::search_item_id("5V aux 输出电流");
        lable_data[index]->setText(label_item_data[index]+ "  " +QString("%1").arg(static_cast<double>(_data.item[_item_id].value))+" A");
    }
    // "3V main 电流:"
    index=search_label_index("3V main 电流:");
    if(LABEL_ITEM_MAX>index)
    {
        _item_id = ipmc_protocolObj::search_item_id("3.3V main 输出电流");
        lable_data[index]->setText(label_item_data[index]+ "  " +QString("%1").arg(static_cast<double>(_data.item[_item_id].value))+" A");
    }
    // "3V aux 电流:"
    index=search_label_index("3V aux 电流:");
    if(LABEL_ITEM_MAX>index)
    {
        _item_id = ipmc_protocolObj::search_item_id("3.3V aux 输出电流");
        lable_data[index]->setText(label_item_data[index]+ "  " +QString("%1").arg(static_cast<double>(_data.item[_item_id].value))+" A");
    }
}

int IPMC::insert_addr(const uint8_t _addr)
{
#if 0
    uint8_t index=0;
    for(index=0; index<sizeof (list_ipmc_addr); index++)
    {
        if(_addr == list_ipmc_addr[index]) break;
        if(0 == list_ipmc_addr[index])
        {
            list_ipmc_addr[index] = _addr;
            goto insert;
        }
    }
    return 0;
insert:
    ui->listWidget_IPMC->clear();
    for(index=0; index<sizeof (list_ipmc_addr); index++)
    {
        if(0 == list_ipmc_addr[index])
        {
            break;
        }
        new QListWidgetItem("ADDR:0x"+QString("%1").arg(list_ipmc_addr[index], 2, 16, QLatin1Char('0')), ui->listWidget_IPMC);
    }
    return 0;
#endif
    int index=0;
    index = _ipmc_protocol.list_insert_addr(_addr);
    if(index<0) return 0;
    ui->listWidget_IPMC->clear();
    for(index=0; index<_ipmc_protocol.list_get_size(); index++)
    {
        if(0 == _ipmc_protocol.list_get_addr(static_cast<uint8_t>(index)) )
        {
            break;
        }
        new QListWidgetItem("ADDR:0x"+QString("%1").arg(_ipmc_protocol.list_get_addr(static_cast<uint8_t>(index)), 2, 16, QLatin1Char('0')), ui->listWidget_IPMC);
    }
    return 0;
}
#if 0
QLabel *IPMC::search_label(const QString description)
{
    uint8_t index=0;
    uint8_t _size = LABEL_ITEM_MAX;// sizeof (label_item_data)/sizeof (label_item_data[0]);
    for(index=0; index<_size; index++)
    {
        if(0==description.compare(label_item_data[index]))
        {
            return lable_data[index];
        }
    }
    return nullptr;
}
#endif
uint8_t IPMC::search_label_index(const QString description)
{
    uint8_t index=0;
    uint8_t _size = LABEL_ITEM_MAX;// sizeof (label_item_data)/sizeof (label_item_data[0]);
    for(index=0; index<_size; index++)
    {
        if(0==description.compare(label_item_data[index]))
        {
            return index;
        }
    }
    return _size;
}
