#ifndef FORM_THREAD_H
#define FORM_THREAD_H

#include <QWidget>

namespace Ui {
class Form_Thread;
}

class Form_Thread : public QWidget
{
    Q_OBJECT

public:
    explicit Form_Thread(QWidget *parent = 0);
    ~Form_Thread();

private:
    Ui::Form_Thread *ui;
};

#endif // FORM_THREAD_H
