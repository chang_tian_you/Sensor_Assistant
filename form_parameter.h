#ifndef FORM_PARAMETER_H
#define FORM_PARAMETER_H

#include <QWidget>

#include "device/interface_sensor.h"

namespace Ui {
class Form_Parameter;
}

class Form_Parameter : public QWidget
{
    Q_OBJECT

public:
    explicit Form_Parameter(QWidget *parent = 0);
    ~Form_Parameter();

    void parameter(struct check_rang_arg * _arg);
    void update_parameter(const struct check_rang_arg * _arg);

private:
    Ui::Form_Parameter *ui;
};

#endif // FORM_PARAMETER_H
