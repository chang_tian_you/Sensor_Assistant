/******************** (C) COPYRIGHT 2018 merafour ********************
* Author             : 冷月追风@merafour.blog.163.com
* Version            : V1.0.0
* Date               : 8/1/2019
* Description        : 通讯接口定义.
********************************************************************************
* merafour.blog.163.com
* merafour@163.com
* github.com/Merafour
*******************************************************************************/

#ifndef QIMSGPORT_H
#define QIMSGPORT_H

#include <QObject>

class QiMSGPort : public QObject
{
    Q_OBJECT
public:
    explicit QiMSGPort(QObject *parent = nullptr);

    virtual int OpenPortDefault(const QString &name) =0;
    virtual void close(void) = 0;
    virtual uint8_t stop(void) = 0;
    virtual void clear(void) = 0;
    virtual void flush(void) = 0;
    virtual qint64 write(const char *data, qint64 len) = 0;
    virtual int read(uint8_t buf[], const int count = 2) = 0;
    virtual uint8_t read(void) = 0;
    virtual int watting_data(const uint32_t timeout) = 0;
signals:

public slots:

protected:
};

#endif // QIMSGPORT_H
