/******************** (C) COPYRIGHT 2018 merafour ********************
* Author             : 冷月追风@merafour.blog.163.com
* Version            : V1.0.0
* Date               : 8/1/2019
* Description        : Device 设备扫描子线程.
********************************************************************************
* merafour.blog.163.com
* merafour@163.com
* github.com/Merafour
*******************************************************************************/
#ifndef SCAN_DEVICE_H
#define SCAN_DEVICE_H

#include <QObject>
#include <QThread>
#include <QDebug>
#include <QMutex>
#include <QStringList>

#include "qimsgport.h"
#include "qimsgsync.h"

class scan_device : public QObject
{
    Q_OBJECT
public:
    explicit scan_device(QObject *parent = nullptr);

    enum Code
    {
        // response codes
        SCAN = 0x0182,   // 扫描设备
        PULL = 0x0185,   // 设备拔出
        IDEL = 0x0187,   // 设备空闲
    };
    static uint8_t is_run(void)
    {
        return _is_run;
    }
    uint8_t stop(void) { return _stop; }
    void stop(uint8_t _s) { _stop = _s; }
    void Serial_Free(const QString COM);
    void scanning(uint8_t _s) { _scanning = _s;}
    static void exit_thread(void) { _exit = 1;}

public slots:
    void doWork(void) ;

signals:
    void sig_scanning_update(const QString &titel, int value);
private:
//    QiSerialSync *scan_port;   // 扫描设备
//    QiSerialSync *pull_port;   // 弹出设备
    QiMSGPort *scan_port;   // 扫描设备
    QiMSGPort *pull_port;   // 弹出设备
    static QStringList port_list;
    static QStringList pull_list;
    //interface_general *interface;
    QiMSGSync *interface;
    static QMutex mutex;
    static uint8_t _is_run;
    static uint8_t _stop;
    static uint8_t _overload;  // 禁止重载
    static uint8_t _scanning;
    static uint8_t _exit;
    void send_idle(QString port);
    int serach_port(QString &port);
};

class ScanningDevice : public QObject
{
    Q_OBJECT
    QThread workerThread;
public:
    ScanningDevice() {
        worker = new scan_device;
        worker->moveToThread(&workerThread);
        connect(&workerThread, &QThread::finished, worker, &QObject::deleteLater);
        connect(this, &ScanningDevice::operate, worker, &scan_device::doWork);
        connect(worker, &scan_device::sig_scanning_update, this, &ScanningDevice::slots_scanning_update);
        workerThread.start();
    }
    ~ScanningDevice() {
        scan_device::exit_thread();
        workerThread.quit();
        workerThread.wait();
    }
    void start(void)
    {
        qDebug() << "Worker Run Thread : " << QThread::currentThreadId();
        emit operate();
        workerThread.setPriority(QThread::TimeCriticalPriority);
    }
    void stop(const uint8_t _s) { worker->stop(_s); }
    void scanning(const uint8_t _s) { worker->scanning(_s); }
    void Serial_Free(const QString COM)
    {
        //qDebug() << "Serial_Free Run Thread : " << QThread::currentThreadId();
        //emit sig_Serial_Free(COM);
        worker->Serial_Free(COM);
    }
public slots:
    void slots_scanning_update(const QString &titel, int value)
    {
        //qDebug() << "ControllerScanning : " << titel;
        emit sig_scanning_update(titel, value);
    }
signals:
    void operate(void);
    void sig_scanning_update(const QString &titel, int value);

private:
    scan_device *worker;
};

#endif // SCAN_DEVICE_H
