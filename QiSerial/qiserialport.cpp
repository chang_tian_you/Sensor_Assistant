/******************** (C) COPYRIGHT 2018 merafour ********************
* Author             : 冷月追风@merafour.blog.163.com
* Version            : V1.0.0
* Date               : 8/1/2019
* Description        : 串口操作.
********************************************************************************
* merafour.blog.163.com
* merafour@163.com
* github.com/Merafour
*******************************************************************************/
#include "qiserialport.h"
#include <QDebug>

QiSerialPort::QiSerialPort(QObject *parent) : QiMSGPort(parent)
{
    _stop = 0;
    queue = new QiQueue();
    //connect(&_Port, &QSerialPort::errorOccurred, this, &QiSerialPort::errorOccurred_slots);
    clear();
}


void QiSerialPort::flush_serial()
{
    //串口部分初始化
    QStandardItemModel *model = new QStandardItemModel(&_Port);
    QStandardItem *item;
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
    {
        qDebug() << "Name         : " << info.portName();
        qDebug() << "Description  : " << info.description();
        qDebug() << "Manufacturer : " << info.manufacturer();
        qDebug() << "Serial number: " << info.serialNumber();

        item = new QStandardItem(info.portName());
        QString comtips="Name:"+info.portName()+"\r\n"+
                "Description:"+info.description()+"\r\n"+
                "Manufacturer:"+info.manufacturer()+"\r\n"+
                "Serial number:"+info.serialNumber();
        item->setToolTip(comtips);                                  //设置提示信息
        model->appendRow(item);
        //qDebug() << comtips;
    }
}

QStringList QiSerialPort::GetPortNames()
{
    QStringList list;

    list.clear();
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
    {
#if 0
        qDebug() << "Name         : " << info.portName();
        qDebug() << "Description  : " << info.description();
        qDebug() << "Manufacturer : " << info.manufacturer();
        qDebug() << "Serial number: " << info.serialNumber();
#endif
        list << info.portName();
    }
    qDebug() << "list         : " << list;
    return list;
}

QString QiSerialPort::search_Description(const QString desc)
{
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
    {
        qDebug() << "Name         : " << info.portName();
        qDebug() << "Description  : " << info.description();
        qDebug() << "Manufacturer : " << info.manufacturer();
        qDebug() << "Serial number: " << info.serialNumber();

        QString comtips="Name:"+info.portName()+"\r\n"+
                "Description:"+info.description()+"\r\n"+
                "Manufacturer:"+info.manufacturer()+"\r\n"+
                "Serial number:"+info.serialNumber();                                 //设置提示信息
        qDebug() << comtips;
        //cbx_com->addItem(info.portName());
        if(0==desc.compare(info.description())) return info.portName();
    }
    return "";
}
/*
 * 打开设备,使用默认参数
*/
int QiSerialPort::OpenPortDefault(const QString &name)
{
    if(_Port.isOpen())
    {
        close();
    }
    //QObject::connect(serial, SIGNAL(readyRead()), this, SLOT(recSerialData())); // 连接串口收到数据事件与读取数据函数
    //connect(this, SIGNAL(readyRead()), this, SLOT(_Read())); // 连接串口收到数据事件与读取数据函数
    //serial->setPortName("COM13");
    _Port.setPortName(name);
    _Port.setBaudRate(QSerialPort::Baud115200);
    //setBaudRate(QSerialPort::Baud57600);
    _Port.setParity(QSerialPort::NoParity);
    _Port.setDataBits(QSerialPort::Data8);
    _Port.setStopBits(QSerialPort::OneStop);
    _Port.setFlowControl(QSerialPort::NoFlowControl);
    if (_Port.open(QIODevice::ReadWrite)) {
        //qDebug()<<"open success" << _Port.readBufferSize();
        //connect(&_Port, &QSerialPort::readyRead, this, &QiSerialPort::recSerialData);
        connect(&_Port, SIGNAL(readyRead()), this, SLOT(recSerialData()));
        _stop = 0;
        return 0;
    } else {
        //qDebug()<<"open failed";
        _stop = 1;
        return -1;
    }
}

void QiSerialPort::errorOccurred_slots(QSerialPort::SerialPortError error)
{
    if(QSerialPort::SerialPortError::TimeoutError == error) return;
    if(QSerialPort::SerialPortError::NoError == error) return;
    //qDebug()<<"error:" << error;
    if(QSerialPort::SerialPortError::DeviceNotFoundError == error) _stop=1;
    if(QSerialPort::SerialPortError::ResourceError == error) _stop=1;
}

