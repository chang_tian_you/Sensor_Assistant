#include "qiserialportbuffer.h"

QiSerialPortBuffer::QiSerialPortBuffer(QObject *parent) : QiMSGPort(parent)
{
    _stop = 0;
    //connect(&_Port, &QSerialPort::errorOccurred, this, &QiSerialPortBuffer::errorOccurred_slots);
    //connect(&_Port, SIGNAL(errorOccurred()), this, SLOT(errorOccurred_slots()));
    clear();
}

int QiSerialPortBuffer::OpenPortDefault(const QString &name)
{
    if(_Port.isOpen())
    {
        close();
    }
    //QObject::connect(serial, SIGNAL(readyRead()), this, SLOT(recSerialData())); // 连接串口收到数据事件与读取数据函数
    //connect(this, SIGNAL(readyRead()), this, SLOT(_Read())); // 连接串口收到数据事件与读取数据函数
    //serial->setPortName("COM13");
    _Port.setPortName(name);
    _Port.setBaudRate(QSerialPort::Baud115200);
    //setBaudRate(QSerialPort::Baud57600);
    _Port.setParity(QSerialPort::NoParity);
    _Port.setDataBits(QSerialPort::Data8);
    _Port.setStopBits(QSerialPort::OneStop);
    _Port.setFlowControl(QSerialPort::NoFlowControl);
    if (_Port.open(QIODevice::ReadWrite)) {
        //qDebug()<<"open success" << _Port.readBufferSize();
        //connect(&_Port, &QSerialPort::readyRead, this, &QiSerialPort::recSerialData);
        connect(&_Port, SIGNAL(readyRead()), this, SLOT(recSerialData()));
        _stop = 0;
        //_rw_flag = 1;
        return 0;
    } else {
        //qDebug()<<"open failed";
        _stop = 1;
        return -1;
    }
}

void QiSerialPortBuffer::errorOccurred_slots(QSerialPort::SerialPortError error)
{
    if(QSerialPort::SerialPortError::TimeoutError == error) return;
    if(QSerialPort::SerialPortError::NoError == error) return;
    //qDebug()<<"error:" << error;
    if(QSerialPort::SerialPortError::DeviceNotFoundError == error) _stop=1;
    if(QSerialPort::SerialPortError::ResourceError == error) _stop=1;
}
