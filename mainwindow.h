#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QProgressBar>
#include <QTime>
#include "scheduling.h"
#include "menu/qitranslator.h"
#include "oscillogram.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    // 菜单
    void actionLanguage_triggered();
    void actionOperate_triggered();
    void actionHelp_triggered();

private:
    // 绘制菜单即操作函数
    //void create_menu(void);
    void create_menu_json(void);
    void Language_cutover(void);
    // 定义菜单
    QMenu *Menu_Operate;
    QAction *actionSearch;
    QAction *actionDisconnect;
    QAction *actionAutoConnect;
    QAction *actionParam;
    QMenu *Menu_File;
    QAction *actionCutover;
    QAction *actionOscillogram;
    QAction *actionLoad;
    QMenu *Menu_Help;
    QAction *actionAbout;

    // 状态栏控件
    QLabel* msgLabel;
    QLabel* permanent;
    QProgressBar* progress;
    // setting
#define Setting_Language     "Language"

    // 数据
    QString fileName;
    int _width;
    int _height;
    QString qss_default;
private:
    Ui::MainWindow *ui;
    scheduling *_scheduling;
    QiTranslator *_cutover;
    static const char *translator_table[]; // 转换表
    Oscillogram *_oscillogram;
};

#endif // MAINWINDOW_H
