/******************** (C) COPYRIGHT 2018 merafour ********************
* Author             : 冷月追风@merafour.blog.163.com
* Version            : V1.0.0
* Date               : 11/1/2019
* Description        : 数据存储接口.
********************************************************************************
* merafour.blog.163.com
* merafour@163.com
* github.com/Merafour
*******************************************************************************/
#ifndef STORAGE_H
#define STORAGE_H

#include <stdint.h>
#include <string.h>
#include <malloc.h>
#include "cJSON.h"

class storage
{
public:
    storage(const char* _objName);
    ~storage()
    {
        if(nullptr!=json_root) free(json_root);
        if(nullptr!=json_item) free(json_item);
    }

    static char* open(const char *path);
    static int opencJSON(const char *path, cJSON* &root);
    static int savecJSON(const char *path, const cJSON* root);

    int opencJSON(const char *path);
    int opencJSON(void)
    {
        return opencJSON(objName);
    }
    void add_item(cJSON* item, const char *string)
    {
        cJSON_AddItemToObject(json_root, string, item);
    }

protected:
    cJSON *json_root;
    cJSON *json_item;
    const char* get_objName(void)
    {
        return objName;
    }

private:
    char objName[64];
    static int create_cutover(const char *path);
};

#endif // STORAGE_H
