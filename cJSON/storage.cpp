/******************** (C) COPYRIGHT 2018 merafour ********************
* Author             : 冷月追风@merafour.blog.163.com
* Version            : V1.0.0
* Date               : 11/1/2019
* Description        : 数据存储接口.
********************************************************************************
* merafour.blog.163.com
* merafour@163.com
* github.com/Merafour
*******************************************************************************/
#include "storage.h"
#include <stdio.h>

storage::storage(const char* _objName)
{
    memset(objName, 0, sizeof(objName));
    if(strlen(_objName)>sizeof (objName))
    {
        memcpy(objName, "storage", strlen("storage"));
    }
    else
    {
        memcpy(objName, _objName, strlen(_objName));
    }
    json_root = nullptr;
    json_item = nullptr;
}

char *storage::open(const char *path)
{
    size_t _size=0;
    size_t r_size=0;
    char* imagebyte=nullptr;

    FILE *fp = nullptr;
    //qDebug() << "open cutover file" ;
    fp = fopen(path, "rb");
    if(nullptr == fp)
    {
        //qDebug() << "Unable to open" << QString::fromLocal8Bit(path);
        goto bad;
    }
    fseek(fp, 0, SEEK_END);
    _size = static_cast<size_t>(ftell(fp));
    if(0!=fseek(fp, 0, SEEK_SET))
    {
        //qDebug() << "Unable to SET" << QString::fromLocal8Bit(path);
        create_cutover(path);  // 文件不存在则创建
        goto bad;
    }
    //qDebug() << "_size:" << _size;
    r_size = _size - _size%1024+2048;
    //if(nullptr == imagebyte) imagebyte = (char*)malloc(r_size);
    if(nullptr == imagebyte) imagebyte = reinterpret_cast<char *>(malloc(r_size));
    if(nullptr == imagebyte)
    {
        //qDebug() << "malloc space fail!";
        goto bad;
    }
    memset(imagebyte, 0x00, r_size);
    r_size = fread(imagebyte, 1, _size, fp);
    //qDebug() << "r_size:" << r_size;
    if(_size != r_size)
    {
        //qDebug() << "read music fail" ;
        goto bad;
    }
    fclose(fp);
    return imagebyte;
bad:
    if(nullptr != fp) fclose(fp);
    return nullptr;
}

int storage::opencJSON(const char *path, cJSON* &root)
{
    cJSON* _root;
    char* imagebyte;
    imagebyte = open(path);
    if(nullptr==imagebyte)
    {
        return -1;
    }
    _root = cJSON_Parse(imagebyte);
    if(nullptr == _root)
    {
        return -2;
    }
    root = _root;
    return 0;
}

int storage::opencJSON(const char *path)
{
    //json_root = cJSON_CreateObject();
    return opencJSON(path, json_root);
}

int storage::savecJSON(const char *path, const cJSON* root)
{
    size_t _size=0;
    size_t w_size=0;
    FILE* fp = nullptr;
    char* json_str;

    json_str = cJSON_Print(root);
    //json_str = cJSON_PrintUnformatted(root);

    fp = fopen(path, "w+");
    if(nullptr == fp)
    {
        //qDebug() << "Unable to open" << QString::fromLocal8Bit(path);
        goto bad;
    }
    if(0!=fseek(fp, 0, SEEK_SET))
    {
        //qDebug() << "Unable to SET" << QString::fromLocal8Bit(path);
        goto bad;
    }
    _size = strlen(json_str);
    //qDebug() << "json size:" << _size;
    w_size = fwrite(json_str, 1, _size, fp);
    //qDebug() << "w_size:" << w_size;
    if(_size != w_size)
    {
        //qDebug() << "write cJSON fail" ;
        goto bad;
    }
    fclose(fp);
    if(nullptr != json_str) free(json_str);
    return 0;

bad:
    if(nullptr != fp) fclose(fp);
    if(nullptr != json_str) free(json_str);
    return -1;
}

int storage::create_cutover(const char *path)
{
    //uint16_t i;
    cJSON* root =  cJSON_CreateObject();
    //cJSON* objroot =  cJSON_CreateObject();
    //char* json_str;

    cJSON_AddItemToObject(root, "file", cJSON_CreateString(path));
    cJSON_AddItemToObject(root, "seeor", cJSON_CreateString("文件不存在"));

    //json_str = cJSON_Print(root);
    //json_str = cJSON_PrintUnformatted(root);
    savecJSON(path, root);

//bad:
    cJSON_Delete(root);
    //free(json_str);
    return 0;
}
