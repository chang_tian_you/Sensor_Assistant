/******************** (C) COPYRIGHT 2018 merafour ********************
* Author             : 冷月追风@merafour.blog.163.com
* Version            : V1.0.0
* Date               : 8/1/2019
* Description        : 子窗口.
********************************************************************************
* merafour.blog.163.com
* merafour@163.com
* github.com/Merafour
*******************************************************************************/

#ifndef IPMC_H
#define IPMC_H

#include <QWidget>
#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include <QListWidget>
#include <QListWidgetItem>
#include <QListView>
#include <QTime>

#include "thread_ipmb.h"
#include "QiSerial/scan_device.h"

namespace Ui {
class IPMC;
}

class IPMC : public QWidget
{
    Q_OBJECT

public:
    explicit IPMC(QWidget *parent = nullptr);
    ~IPMC();

    void set_title(QString title);
    QString get_title(void)
    {
        return _title;
    }
    //void handle(QString COM, ControllerScanning *_scan);
    void handle(QString COM, ScanningDevice *_scan);
    int idle(QString COM);
    uint8_t busy(void)
    {
        return _busy;
    }
    void Language(QTranslator &_t);
    void stop(void)
    {
        ipmb_cpu->stop();
    }
    void search_device(void)
    {
        ipmb_cpu->search_device();
    }

private slots:
    void on_listWidget_IPMC_clicked(const QModelIndex &index);


    void slots_progress_update(const QString &titel, int value);

private:
    Ui::IPMC *ui;

    void ipmc_data_handle(QString slave);
    int insert_addr(const uint8_t _addr);
    //QLabel* search_label(const QString description);
    uint8_t search_label_index(const QString description);

#define LABEL_ITEM_MAX  15
    QLabel *lable_data[20];
    static const QString label_item_data[20];
    thread_ipmb_controller *ipmb_cpu;
    ipmc_protocolObj _ipmc_protocol;
    uint8_t _busy;
    QString _title;
    QString qss_default;
    //ControllerScanning *_scanning;
    ScanningDevice *_scanning_device;
    qint64 timestamp;
};

#endif // IPMC_H
