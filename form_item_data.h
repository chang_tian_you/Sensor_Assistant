#ifndef FORM_ITEM_DATA_H
#define FORM_ITEM_DATA_H

#include <QWidget>
#include <QString>
#include <QTranslator>
#include "device/interface_sensor.h"

namespace Ui {
class Form_Item_Data;
}

class Form_Item_Data : public QWidget
{
    Q_OBJECT

public:
    explicit Form_Item_Data(QWidget *parent = 0);
    ~Form_Item_Data();

    void set_title(QString title);
    void Language(QTranslator &_t)
    {
        (void) _t;
    }
    void update(const struct interface_data *_data);

private:
    Ui::Form_Item_Data *ui;
    QString _title;

    QString qs_float(const float value);
    QString qs_hex(const uint8_t addr);
};

#endif // FORM_ITEM_DATA_H
