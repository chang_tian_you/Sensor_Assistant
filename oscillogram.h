#ifndef OSCILLOGRAM_H
#define OSCILLOGRAM_H

#include <QMainWindow>
#include "qcustomplot.h"
#include <QTimer>
#include <QColor>

namespace Ui {
class Oscillogram;
}

class Oscillogram : public QMainWindow
{
    Q_OBJECT

public:
    explicit Oscillogram(QWidget *parent = nullptr);
    ~Oscillogram();

public slots:
    void realtimeDataSlot(void);
    void bracketDataSlot(void);
private:
    Ui::Oscillogram *ui;
    QCustomPlot *customPlot;
    QTimer dataTimer;
    QCPItemTracer *itemDemoPhaseTracer;

    // 官方历程
    void Simple_Demo(void);
    // 散射风格演示
    void Scatter_Style_Demo(void);
    void Scatter_Style_Test(void);
    // 实时数据演示
    void Realtime_Data_Demo(void);
    // 时间轴演示
    void Date_Axis_Demo(void);
    // Sinc散射演示
    void Sinc_Scatter_Demo();
    void Item_Demo(void);
};

#endif // OSCILLOGRAM_H
