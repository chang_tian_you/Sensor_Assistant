#include "form_parameter.h"
#include "ui_form_parameter.h"
#include <QRegExp>
#include <QDebug>
#include <QRegExpValidator>
Form_Parameter::Form_Parameter(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form_Parameter)
{
    ui->setupUi(this);
    QRegExp regx("[0-9]+$");
    QValidator *validator = new QRegExpValidator(regx, this );
    ui->lineEdit_accel_min->setValidator( validator );
    ui->lineEdit_accel_max->setValidator( validator );
    ui->lineEdit_mbar_min->setValidator( validator );
    ui->lineEdit_mbar_max->setValidator( validator );
    ui->lineEdit_temp_min->setValidator( validator );
    ui->lineEdit_temp_max->setValidator( validator );
    ui->lineEdit_temp_diff->setValidator( validator );
    ui->lineEdit_accel_min->setText("970");
    ui->lineEdit_accel_max->setText("1030");
    ui->lineEdit_mbar_min->setText("98000");
    ui->lineEdit_mbar_max->setText("1080000");
    ui->lineEdit_temp_min->setText("350");
    ui->lineEdit_temp_max->setText("510");
    ui->lineEdit_temp_diff->setText("20");
}

Form_Parameter::~Form_Parameter()
{
    delete ui;
}

void Form_Parameter::parameter(check_rang_arg *_arg)
{
    _arg->accel_min = ui->lineEdit_accel_min->text().toInt();
    _arg->accel_max = ui->lineEdit_accel_max->text().toInt();
    _arg->mbar_min = ui->lineEdit_mbar_min->text().toInt();
    _arg->mbar_max = ui->lineEdit_mbar_max->text().toInt();
    _arg->temp_min = ui->lineEdit_temp_min->text().toInt();
    _arg->temp_max = ui->lineEdit_temp_max->text().toInt();
    _arg->temp_diff = ui->lineEdit_temp_diff->text().toInt();
    //qDebug() << "_arg->accel_min:" << _arg->accel_min;
}

void Form_Parameter::update_parameter(const check_rang_arg *_arg)
{
    ui->lineEdit_accel_min->setText(QString::number(_arg->accel_min));
    ui->lineEdit_accel_max->setText(QString::number(_arg->accel_max));
    ui->lineEdit_mbar_min->setText(QString::number(_arg->mbar_min));
    ui->lineEdit_mbar_max->setText(QString::number(_arg->mbar_max));
    ui->lineEdit_temp_min->setText(QString::number(_arg->temp_min));
    ui->lineEdit_temp_max->setText(QString::number(_arg->temp_max));
    ui->lineEdit_temp_diff->setText(QString::number(_arg->temp_diff));
}
