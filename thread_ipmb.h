/******************** (C) COPYRIGHT 2018 merafour ********************
* Author             : 冷月追风@merafour.blog.163.com
* Version            : V1.0.0
* Date               : 8/1/2019
* Description        : 设备通讯线程.
********************************************************************************
* merafour.blog.163.com
* merafour@163.com
* github.com/Merafour
*******************************************************************************/
#ifndef THREAD_IPMB_H
#define THREAD_IPMB_H

#include <QObject>
#include <QThread>
#include <QDebug>

#include "device/interface_ipmc.h"
#include "device/ipmc_protocol.h"
#include "QiSerial/qimsgport.h"
#include "QiSerial/qimsgsync.h"
#include "cJSON/cjsonstorage.h"

class thread_ipmb : public QObject
{
    Q_OBJECT
public:
    //explicit thread_ipmb(QObject *parent = nullptr);
    //explicit thread_ipmb(QObject *parent = nullptr, struct ipmb_cpu_data &_data);
    explicit thread_ipmb(QObject *parent, ipmc_protocolObj &_ipmc)  : QObject(parent), _ipmc_protocol(_ipmc)//, _cpu_data(_data)
    {
        //vpx_solt_id = 0;
        //slave_addr=0xEE;
        _storage = new cJSONstorage("ipmb.txt");
        _storage->opencJSON();
    }

    static void exit_thread(void) { _exit = 1;}
    void stop(void) { _stop = 1;}
    int get_ipmc_data_faddr(struct ipmc_data &_data, const uint8_t slave_addr)
    {
        return _ipmc_protocol.get_ipmc_data_faddr(_data, slave_addr);
    }
    void search_device(void)
    {
        _search_flag = 1;
    }

signals:
    void sig_progress_update(const QString &titel, int value);

public slots:
    void doWork(const QString COM);
    void slots_progress_update(const QString &titel, int value)
    {
        emit sig_progress_update(titel, value);
    }

private:
    void loop(void);
    void master_test(const uint8_t slave_addr, const uint8_t _cmd);
    void search_ipmc(void);
    void storage_data(const uint8_t slave_addr, const uint32_t count);
    static uint8_t _exit; // Thread exit
    ipmc_protocolObj &_ipmc_protocol;
    //user_port *_port;
    QiMSGPort *_port;
    interface_ipmc *interface;
    cJSONstorage *_storage;
    uint8_t _stop;        // The current task exit
    //uint8_t slave_addr;
    //uint8_t vpx_solt_id;
    uint8_t _search_flag;
    uint8_t _rev[6];      // 对齐

};

class thread_ipmb_controller : public QObject
{
    Q_OBJECT
    //QThread workerThread;
public:
    thread_ipmb_controller(ipmc_protocolObj &_ipmc)
    {
//        worker = new thread_ipmb;
        worker = new thread_ipmb(nullptr, _ipmc);
        worker->moveToThread(&workerThread);
        connect(&workerThread, &QThread::finished, worker, &QObject::deleteLater);
        //connect(this, &ControllerProgram::operate, worker, &Program::doWork);
        connect(this, SIGNAL(operate(QString)), worker, SLOT(doWork(QString)));
        connect(worker, SIGNAL(sig_progress_update(const QString &, int)), this, SLOT(slots_progress_update(const QString &, int)));
        workerThread.start();
    }
    ~thread_ipmb_controller() {
        worker->exit_thread();
        workerThread.quit();
        workerThread.wait();
    }
    void start(const QString COM)
    {
        qDebug() << "Worker Run Thread : " << QThread::currentThreadId();
        emit operate(COM);
    }
    void stop(void)
    {
        worker->stop();
    }
    int get_ipmc_data_faddr(struct ipmc_data &_data, const uint8_t slave_addr)
    {
        return worker->get_ipmc_data_faddr(_data, slave_addr);
    }
    void search_device(void)
    {
        worker->search_device();
    }
public slots:
    void slots_progress_update(const QString &titel, int value)
    {
        //qDebug() << "slots_progress_update " << titel;
        emit sig_progress_update(titel, value);
    }
signals:
    void operate(const QString);
    void sig_progress_update(const QString &titel, int value);
private:
    thread_ipmb *worker;
    QThread workerThread;
};

#endif // THREAD_IPMB_H
