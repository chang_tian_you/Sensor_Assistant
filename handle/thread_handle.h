#ifndef THREAD_HANDLE_H
#define THREAD_HANDLE_H

#include <QObject>
#include <QDebug>
#include "QiSerial/qimsgport.h"
#include "QiSerial/qimsgsync.h"
#include "cJSON/cjsonstorage.h"
#include "device/interface_sensor.h"

class thread_handle : public QObject
{
    Q_OBJECT
public:
    explicit thread_handle(struct interface_data *_data, QObject *parent = nullptr);

    static void exit_thread(void) { _exit = 1;}
    void stop(void) { _stop = 1;}
//    int get_ipmc_data_faddr(struct ipmc_data &_data, const uint8_t slave_addr)
//    {
//        return _ipmc_protocol.get_ipmc_data_faddr(_data, slave_addr);
//    }
//    void search_device(void)
//    {
//        _search_flag = 1;
//    }
    static struct check_rang_arg* param(void)
    {
        return &_param;
    }
    void write_param(void)
    {
        _param_flag = 1;
    }

signals:
    void sig_progress_update(const QString &titel, int value);

public slots:
    void doWork(const QString COM);
    void slots_progress_update(const QString &titel, int value)
    {
        emit sig_progress_update(titel, value);
    }

private:
    void loop(void);
    int search_index(const char* name);
//    void master_test(const uint8_t slave_addr, const uint8_t _cmd);
//    void search_ipmc(void);
//    void storage_data(const uint8_t slave_addr, const uint32_t count);
    static uint8_t _exit; // Thread exit
    //user_port *_port;
    QiMSGPort *_port;
    interface_sensor *interface;
    cJSONstorage *_storage;
    struct interface_data *_interface_data;
    static struct check_rang_arg _param;
    uint8_t _param_flag;
    uint8_t _stop;        // The current task exit
    //uint8_t slave_addr;
    //uint8_t vpx_solt_id;
    uint8_t _search_flag;
    uint8_t _rev[6];      // 对齐
};

class thread_handle_controller : public QObject
{
    Q_OBJECT
    //QThread workerThread;
public:
    thread_handle_controller(struct interface_data *_data)
    {
//        worker = new thread_ipmb;
        worker = new thread_handle(_data);
        worker->moveToThread(&workerThread);
        connect(&workerThread, &QThread::finished, worker, &QObject::deleteLater);
        //connect(this, &ControllerProgram::operate, worker, &Program::doWork);
        connect(this, SIGNAL(operate(QString)), worker, SLOT(doWork(QString)));
        connect(worker, SIGNAL(sig_progress_update(const QString &, int)), this, SLOT(slots_progress_update(const QString &, int)));
        workerThread.start();
    }
    ~thread_handle_controller() {
        worker->exit_thread();
        workerThread.quit();
        workerThread.wait();
    }
    void start(const QString COM)
    {
        qDebug() << "Worker Run Thread : " << QThread::currentThreadId();
        emit operate(COM);
    }
    void stop(void)
    {
        worker->stop();
    }
//    int get_ipmc_data_faddr(struct ipmc_data &_data, const uint8_t slave_addr)
//    {
//        return worker->get_ipmc_data_faddr(_data, slave_addr);
//    }
//    void search_device(void)
//    {
//        worker->search_device();
//    }
    void write_param(void)
    {
        worker->write_param();
    }
public slots:
    void slots_progress_update(const QString &titel, int value)
    {
        //qDebug() << "slots_progress_update " << titel;
        emit sig_progress_update(titel, value);
    }
signals:
    void operate(const QString);
    void sig_progress_update(const QString &titel, int value);
private:
    thread_handle *worker;
    QThread workerThread;
};

#endif // THREAD_HANDLE_H
