#include "about.h"
#include "ui_about.h"
#include <QDate>
#include <QTime>
#include <stdio.h>
//#define _VERSION_REVR							0x02
//#define _VERSION_REVX							0x00
//#define _VERSION_REVY							0x0A

extern "C"
{
extern const char* get_build_data(void);
extern const char* get_build_time(void);
extern const char* get_version(void);
}

const uint8_t About::_VERSION_REVR=0x02;
const uint8_t About::_VERSION_REVX=0x00;
const uint8_t About::_VERSION_REVY=0x0A;

//static const QDate buildDate = QLocale( QLocale::English ).toDate( QString( __DATE__ ).replace( "  ", " 0" ), "MMM dd yyyy");
//static const QTime buildTime = QTime::fromString( __TIME__, "hh:mm:ss" );


const char* About::translator_table[] =
{
    // title
    "IPMB Assistant",
};

About::About(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::About)
{
    char time[256];
    ui->setupUi(this);
    _cutover = new QiTranslator("About", translator_table, sizeof (translator_table)/sizeof (translator_table[0]));
    _cutover->load();

    setWindowTitle(_cutover->cutover("IPMB Assistant"));
    //setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowMinimizeButtonHint | Qt::WindowMaximizeButtonHint);
    setWindowFlags(Qt::WindowCloseButtonHint);
    //ui->buttonBox->setStandardButtons(QDialogButtonBox::Ok);
    ui->buttonBox->setStandardButtons(QDialogButtonBox::Ok);
    //ui->groupBox_version->setTitle("MusicPlay");
    ui->groupBox_version->setTitle("merafour.blog.163.com");
    //ui->label_version->setText("  Version: V1.12.0");
    //ui->label_version->setText("  Version: V "+QString::number(_BL_REVR) +"."+QString::number(_BL_REVX) +"."+QString::number(_BL_REVY) +" ");
    ui->label_version->setText(version());
    memset(time, 0, sizeof(time));
    snprintf(time, sizeof(time)-1, "  %s %s", get_build_data(), get_build_time());
    ui->label_time->setText(time);
    ui->textBrowser_version->setEnabled(false);
    ui->textBrowser_version->setText("更新：");
    ui->textBrowser_version->append("1.自动识别设备");
    ui->textBrowser_version->append("2.协议转发");
    ui->textBrowser_version->append("3.增加数据存储功能");
    adjustSize();
    setFixedSize(this->width(), this->height());
}

About::~About()
{
    delete ui;
}

QString About::version()
{
#if 0
    QString _version = "  Version: V "+QString::number(_VERSION_REVR) +"."+QString::number(_VERSION_REVX) +"."+QString::number(_VERSION_REVY) +" ";
    return _version;
#else
    QString _version = "  Version: "+QString::fromUtf8(get_version());
    return _version;
#endif
}
