#include <stdio.h>
#include <stdint.h>
#include <string.h>

#define version_revr   0
static char _ver[128]={0};
static char _data[128]={0};
static char _time[128]={0};

const char* get_build_data(void)
{
    memset(_data, 0, sizeof (_data));
    memcpy(_data, __DATE__, strlen(__DATE__));
    return _data;
}
const char* get_build_time(void)
{
    memset(_time, 0, sizeof (_time));
    memcpy(_time, __TIME__, strlen(__TIME__));
    return _time;
}

const char* get_version(void)
{
    char month[8];
    uint32_t year;
    uint32_t day;
    memset(month, 0, sizeof (month));
    if('V'!=_ver[0]) memset(_ver, 0, sizeof (_ver));
    sscanf(__DATE__, "%s %d %d", month, &day, &year);
    sprintf(_ver, "V%d%s.%d.%d", (year%100), month, day, version_revr);
    return _ver;
}
