#ifndef ABOUT_H
#define ABOUT_H

#include <QDialog>
#include "qitranslator.h"

namespace Ui {
class About;
}

class About : public QDialog
{
    Q_OBJECT

public:
    explicit About(QWidget *parent = nullptr);
    ~About();
    static QString version(void);

private:
    Ui::About *ui;

    const static uint8_t _VERSION_REVR;
    const static uint8_t _VERSION_REVX;
    const static uint8_t _VERSION_REVY;

    QiTranslator *_cutover;
    static const char *translator_table[]; // 转换表
};

#endif // ABOUT_H
