/******************** (C) COPYRIGHT 2018 merafour ********************
* Author             : 冷月追风@merafour.blog.163.com
* Version            : V1.0.0
* Date               : 8/1/2019
* Description        : 语言转换.
********************************************************************************
* merafour.blog.163.com
* merafour@163.com
* github.com/Merafour
*******************************************************************************/
#ifndef QITRANSLATOR_H
#define QITRANSLATOR_H

#include <QObject>
#include <QString>
#include "cJSON/cJSON.h"

class QiTranslator : public QObject
{
    Q_OBJECT
public:
    //explicit QiTranslator(QObject *parent = nullptr);
    explicit QiTranslator(QString _objName, const char** _table, const uint16_t _size, QObject *parent = nullptr);
    ~QiTranslator()
    {
        if(nullptr!=json_root) cJSON_Delete(json_root);
    }

signals:

public slots:

public:
    void load(void);
    const QString cutover(const QString _text);

    char* open(const char *path);
    int create_cutover(const char *path);
    static int savecJSON(const char *path, const char* json_str);
    static const char cutover_file_path[32];
    cJSON* json_root;
    cJSON* json_obj_root;
    QString objName;
    const char** translator_table; // 转换表
    const uint16_t translator_size; // 转换表大小
};

#endif // QITRANSLATOR_H
